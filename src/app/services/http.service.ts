import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Md5} from 'ts-md5/dist/md5';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  API_ENDPOINT = 'http://gateway.marvel.com/v1/public';
  md5 = new Md5();
  ts = Math.floor(Number(new Date())).toString();
  private publickey: "96400096c9c16760c0ecf07d4687de21";
  private privatekey: "6363da0403245708b05a56be82c502ee754d72fb";


  constructor(private http: HttpClient) {
  }
  
  
  getCharacters(){

    this.http.get(this.API_ENDPOINT + '/comics', {
      params: {
        ts: Math.floor(Number(new Date())).toString(), 
        apikey: '96400096c9c16760c0ecf07d4687de21', 
        hash: Md5.hashStr(Math.floor(Number(new Date())).toString().concat("6363da0403245708b05a56be82c502ee754d72fb").concat("96400096c9c16760c0ecf07d4687de21")).toString().trim().toLowerCase()}})
      .subscribe( response => {
      console.log(response);
    });

  }
}
